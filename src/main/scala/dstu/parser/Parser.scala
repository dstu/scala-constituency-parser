package dstu.parser

object ReadTrees extends App {
  import tree.TreeReader

  def usage = throw new IllegalArgumentException("Usage: ReadTrees <infiles>")

  if (args.size < 1) usage

  args.foreach(io.Source.fromFile(_).getLines.foreach(s => println(TreeReader(s))))
}

object ReadBinarizedTrees extends App {
  import tree.TreeReader
  import tree.Tree

  def usage = throw new IllegalArgumentException("Usage: ReadBinarizedTrees <infiles>")

  if (args.size < 1) usage

  args.foreach(io.Source.fromFile(_).getLines.foreach(s => println(Tree.leftBinarize(TreeReader(s)))))
}

object ParseFromRules extends App {
  import tree.TreeReader
  import tree.Tree
  import cky.Parser

  def usage = throw new IllegalArgumentException("Usage: ReadBinarizedPcfg <root symbol> <rules file>")

  if (args.size < 2) usage

  val root = args(0)

  print("Loading grammar...")
  val grammar = dstu.parser.transform.UnaryCollapse(BinarizedPcfg.load(root, new java.io.File(args(1))))
  println(" done")

  val parser = Parser(grammar)

  var input = Console.readLine
  while (null != input) {
    val start = System.currentTimeMillis
    print("Parsing...")
    // val chart = parser.parse(input.split("""\s+"""))
    val tree = parser.parseTree(input.split("""\s+"""))
    val end = System.currentTimeMillis
    println(" done (%.5f s).".format((end - start).toFloat / 1000))
    tree match {
      case Some(t) => println(Tree.lift(t))
      case None => println()
    }
    // println("Tree: " + tree)
    // println("Chart: " + chart)
    input = Console.readLine
  }
}

object Parse extends App {
  import tree.{ Tree, TreeReader }
  import cky.Parser

  def usage = throw new IllegalArgumentException("Usage: Parse <root symbol> <infiles>")

  if (args.size < 2) usage

  val root = args(0)

  def grammar = {
    print("Reading trees...")
    var treeCount = 1
    var binarizedTreeCount = 1
    val binarizedTrees = for (line <- args.tail.flatMap(io.Source.fromFile(_).getLines);
                              tree = TreeReader(line)) yield Tree.leftBinarize(tree)
    // val trees = args.tail.flatMap(io.Source.fromFile(_).getLines).map(l => {
    //   val t = TreeReader(l)
    //   if (0 == treeCount % 100) println("Read tree " + treeCount)
    //   treeCount = treeCount + 1
    //   t
    // }).map(t => {
    //   val x = Tree.leftBinarize(t)
    //   if (0 == binarizedTreeCount % 100) println("Binarized tree " + binarizedTreeCount)
    //   binarizedTreeCount = binarizedTreeCount + 1
    //   x
    // })
    println(" done.")
    print("Building grammar...")
    val result = dstu.parser.transform.UnaryCollapse(BinarizedPcfg(root, binarizedTrees))
    println(" done.")
    result
  }

  val parser = Parser(grammar)

  var input = Console.readLine
  while (null != input) {
    val start = System.currentTimeMillis
    print("Parsing...")
    val chart = parser.parse(input.split("""\s+"""))
    val tree = parser.parseTree(input.split("""\s+"""))
    val end = System.currentTimeMillis
    println(" done (%.5f s).".format((end - start).toFloat / 1000))
    tree match {
      case Some(t) => println(Tree.lift(t))
      case None => println()
    }
    // println("Tree: " + tree)
    // println("Chart: " + chart)
    input = Console.readLine
  }
}

// object CkyTypes {
//   case class Span(start: Int, end: Int)
//   case class CkyCellValue[A](val symbol: A,
//                              val probability: Probability,
//                              val backPointers: List[CkyCellValue[A]])
//   type CkyCellLookup[A] = Map[A, CkyCellValue[A]]
//   type CkyParseChart[A] = Map[Span, CkyCellLookup[A]]
// }

// import CkyTypes._

// object CkyParser {
//   def parse[A](tokens: List[A], grammar: Pcfg[A]): CkyParseChart[A] = new CkyParser[A](grammar).parse(tokens)
  // def apply[A](tokens: Seq[A], grammar: Pcfg[A]): Option[Tree[A]] =
  //   parse(tokens, grammar).tree(Span(0, tokens.size + 1), g.rootSymbol)
// }

// object CkyParseChart[A](val tokens: List[A]) {

// }

// type CkyParseChart[A](val tokens: List[A],
//                        val cells: Map[Span, CkyCellLookup[A]]) {
//   val size: Int = tokens.size

//   private def toTree(cell: CkyCellValue[A]): Tree[A] = {
//     Tree(cell.symbol, cell.backPointers.map(toTree(_)))
//   }

//   def tree(s: Span, symbol: A): Option[Tree[A]] = {
//     cells.get(s) match {
//       case Some(cellLookup) => {
//         cellLookup.get(symbol) match {
//           case Some(cellValue) => Some(toTree(cellValue))
//           case None => None
//         }
//       }
//       case None => None
//     }
//   }
// }

// object CkyParseChart {
//   def apply[A](tokens: List[A]): CkyParseChart[A] = {
//     val tokenSpans = for (spanStart <- 0 to (tokens.size - 1);
//                           spanEnd = spanStart + 1;
//                           token = tokens(spanStart);
//                           span = Span(spanStart, spanEnd);
//                           cellLookup = Map[A, CkyCellValue[A]](token -> CkyCellValue(token, Probability.One, Nil)))
//                      yield Pair(span, cellLookup)
//     val emptySpans = for (spanLength <- 2 to tokens.size;
//                           spanStart <- 0 to (tokens.size - spanLength);
//                           spanEnd = spanStart + spanLength;
//                           span = Span(spanStart, spanEnd);
//                           cellLookup = Map[A, CkyCellValue[A]]()) yield Pair(span, cellLookup)
//     new CkyParseChart[A](tokens, (tokenSpans ++ emptySpans).toMap)
//   }
// }

// class CKYParseChart(val terminalCount: Int,
//                     val cells: Map[Edge, CKYCellValue]) extends ParseChart {
//   override val size = terminalCount

//   def edges(): Iterable[Edge] = cells.map(_._1)

//   def probability(e: Edge): Double = {
//     cells.get(e) match {
//       case None => 0.0
//       case Some(cell) => cell.probability
//     }
//   }

//   def combine(pairs: Collection[Pair[Edge, CKYCellValue]]): (CKYParseChart, Set[String]) = {
//     val newSymbols = new scala.collection.mutable.HashSet[String]()
//     val newCells = (cells /: pairs){ (map, pair) =>
      // FIXME: tie-breaking goes here
// //       if (pair._2.probability > map.get(pair._1).getOrElse(CKYCellValue(0.0, List[Edge]())).probability) {
// //         newSymbols += pair._1.symbol
// //         map + pair
// //       } else {
// //         map
// //       }
// //     }
// //     (CKYParseChart(terminalCount, newCells),
// //      Set[String]() ++ newSymbols)
// //   }

//   def combine(other: CKYParseChart): (CKYParseChart, Set[String]) = combine(other.cells)

//   def tree(e: Edge): Option[ParseTree] = {
//     if (!cells.contains(e))
//       None
//     else
//       Some(ParseTree(e.symbol, cells.get(e).get.backPointers.map( (e) => tree(e).get)))
//   }
// }

// object CKYParseChart {
//   def apply(s: Int, m: Map[Edge, CKYCellValue]): CKYParseChart = {
//     new CKYParseChart(s, m)
//   }

//   def apply(words: Collection[String]): CKYParseChart = {
//     val s = words.size
//     val wordsArray = words.toArray
//     val baseCells: Map[Edge, CKYCellValue] =
//       Map[Edge, CKYCellValue]() ++ (0 until s).map( (i) =>
//         Edge(wordsArray(i), i, i + 1)).map( (w) =>
//           (w, CKYCellValue(1.0, List[Edge]())))
//     new CKYParseChart(s, baseCells)
//   }
// }

// abstract class ChartParser(val grammar: Grammar) {
//   def parse(words: Collection[String]): ParseChart
// }

// object CKYParser {
//   def apply(g: Grammar) = new CKYParser(g)
// }

// /** events:
//  * new edge for symbol found (binary production)
//  * new edge for symbol found (unary production)
//  *
//  * ordering:
//  * need to get all binary edges for spans of length s before getting unary edges over them
//  * assumption of treebank-style grammar (all terminals produced by unary productions) in effect!!
//  * */
// class CKYParser(override val grammar: Grammar) extends ChartParser(grammar) {
//   private val unaryProductionsByChild: Map[String, Set[UnaryProduction]] =
//     (Map[String, Set[UnaryProduction]]() /: grammar.unaryProductions)( (map, prod) =>
//       map + Pair(prod.child, map.get(prod.child).getOrElse(Set[UnaryProduction]()) + prod))

//   def parse(words: Collection[String]): CKYParseChart = {
//     parse(words, words.size)
//   }

//   def parse(words: Collection[String], spanLength: Int): CKYParseChart = {
//     if (1 == spanLength) {
//       unaryFill(1, Pair(CKYParseChart(words), Set[String]() ++ words))
//     } else {
//       unaryFill(spanLength, binaryFill(parse(words, spanLength - 1), spanLength))
//     }
//   }

//   /** Evaluates to table filled in from binary productions for
//       edges of length s and set of symbols which were added over spans. */
//   private def binaryFill(table: CKYParseChart, s: Int): (CKYParseChart, Set[String]) = {
//     // println("binaryFill: segment length " + s + ", " + table.cells.size + " edges")

//     table.combine(for {
//       r <- grammar.binaryProductions
//       i <- 0 to table.terminalCount - s
//       j <- i + 1 until i + s
//       leftChildEdge = Edge(r.leftChild, i, j)
//       rightChildEdge = Edge(r.rightChild, j, i + s)
//       if table.cells.contains(leftChildEdge)
//       if table.cells.contains(rightChildEdge)
//       newEdge = Edge(r.parent, i, i + s)
//       newProb = grammar.probability(r) * table.probability(leftChildEdge) * table.probability(rightChildEdge)
//       // FIXME: this is very inefficient. (we should point to existing edges)
// //       childPointers = leftChildEdge :: rightChildEdge :: List[Edge]()
// //     } yield (Pair(newEdge, CKYCellValue(newProb, childPointers))))
// //   }

//   /** Evaluates to table filled in from unary productions for
//       edges of length s. */
//   def unaryFill(s: Int, pair: Pair[CKYParseChart, Set[String]]): CKYParseChart = {
//     val table: CKYParseChart = pair._1
//     val available: Set[String] = pair._2
//     // println("unaryFill: segment length " + s + ", " + table.cells.size + " edges, " + available.size + " available nodes")
// //     if (available.isEmpty) {
// //       table
// //     } else {
// //       unaryFill(s, table.combine((for {
// //         a <- available
// //         r <- unaryProductionsByChild.get(a).getOrElse(Set[UnaryProduction]())
// //         i <- 0 to table.terminalCount - s
// //         childEdge = Edge(a, i, i + s)
// //         if table.cells.contains(childEdge)
// //         // FIXME: this is very inefficient. (we should point to existing edges)
// //         childPointer = childEdge :: List[Edge]()
// //         newEdge = Edge(r.parent, i, i + s)
// //         newProb = grammar.probability(r) * table.probability(childEdge)
// //       } yield (Pair(newEdge, CKYCellValue(newProb, childPointer))))))
// //     }
// //   }
// // }
