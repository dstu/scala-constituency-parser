package dstu.parser
package object math {
  import Numeric.Implicits._
  import dstu.parser.math.Implicits._

  def product[A: Numeric](as: Traversable[A]): A =
    if (as.size < 1) {
      implicitly[Numeric[A]].zero
    } else {
      as.foldLeft(implicitly[Numeric[A]].one)(_ * _)
    }

  def sum[A: Numeric](as: Traversable[A]): A =
    if (as.size < 1) {
      implicitly[Numeric[A]].zero
    } else {
      as.foldLeft(implicitly[Numeric[A]].zero)(_ + _)
    }

  def normalize[A: Fractional](as: Seq[A]): Seq[A] =
    if (as.size < 1) {
      as
    } else {
      val total = sum(as)
      as.map(implicitly[Fractional[A]].div(_, total))
    }

  // def normalize[A <: Measured](as: Seq[A]): Seq[A] =
  //   if (as.size < 1) {
  //     as
  //   } else {
  //     val total = sum(as.map(_.probability))
  //     as.map((m) => m.withProbability(m.probability / total))
  //   }
}

