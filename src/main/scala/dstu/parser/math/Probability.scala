package dstu.parser.math
import scala.math.Ordering
import scala.math.{ log, exp }

class Probability(private val value: Double) {
  import Probability.{ One, Zero }

  private def orderedApply(f: (Double, Double) => Double)(x: Double, y: Double) =
    if (x < y) {
      f(y, x)
    } else {
      f(x, y)
    }

  private def logSum(larger: Double, smaller: Double): Double = log(1 + exp(larger - smaller)) + smaller

  private def logDifference(larger: Double, smaller: Double): Double = log(1 - exp(larger - smaller)) + smaller

  def +(x: Probability): Probability = new Probability(orderedApply(logSum)(value, x.value))
  def -(x: Probability): Probability = new Probability(orderedApply(logDifference)(value, x.value))
  def *(x: Probability): Probability = new Probability(value + x.value)
  def /(x: Probability): Probability =
    x match {
      case Probability.Zero => throw new IllegalArgumentException("Division by zero")
      case _ => new Probability(value - x.value)
    }
  def ==(d: Probability) = value == d.value
  def <(d: Probability) = value < d.value
  def <=(d: Probability) = value <= d.value
  def >(d: Probability) = value > d.value
  def >=(d: Probability) = value >= d.value
  def compare(d: Probability) = Ordering.Double.compare(value, d.value)
  def toDouble = exp(value)
  def toFloat = toDouble.toFloat
  def toInt = toDouble.toInt
  def toLong = toDouble.toLong
  def equals(d: Probability) = value == d.value
  override def hashCode = value.hashCode
  override def toString = value.toString
}

object Probability {
  val Zero: Probability = new Probability(Double.NegativeInfinity)
  val One: Probability = new Probability(0.0)

  def apply(d: Double): Probability = {
    if (d > 1.0) {
      throw new IllegalArgumentException("Probability " + d + " greater than 1.0")
    } else if (d < 0.0) {
      throw new IllegalArgumentException("Probability " + d + " less than 0.0")
    } else if (d != d) {
      throw new IllegalArgumentException("Invalid probability value " + d)
    } else {
      d match {
        case 1.0d => One
        case 0.0d => Zero
        case _ => new Probability(log(d))
      }
    }
  }

  def apply(i: Int): Probability = Probability(i.toDouble)
}

trait ProbabilityOrdering extends Ordering[Probability] {
  def compare(x: Probability, y: Probability) = x.compare(y)
}

trait ProbabilityIsConflicted extends Numeric[Probability] {
  def plus(x: Probability, y: Probability): Probability = x + y
  def minus(x: Probability, y: Probability): Probability = x - y
  def times(x: Probability, y: Probability): Probability = x + y
  def negate(x: Probability): Probability = throw new UnsupportedOperationException
  override def zero: Probability = Probability.Zero
  override def one: Probability = Probability.One
  override def signum(x: Probability): Int = {
    x match {
      case Probability.Zero => 0
      case _ => 1
    }
  }
  def fromInt(x: Int): Probability = Probability(x)
  def toInt(x: Probability): Int = x.toInt
  def toLong(x: Probability): Long = x.toLong
  def toFloat(x: Probability): Float = x.toFloat
  def toDouble(x: Probability): Double = x.toDouble
}

trait ProbabilityIsFractional extends ProbabilityIsConflicted with Fractional[Probability] {
  def div(x: Probability, y: Probability): Probability = x / y
}

trait ProbabilityAsIfIntegral extends ProbabilityIsConflicted with Integral[Probability] {
  def quot(x: Probability, y: Probability): Probability = x / y
  def rem(x: Probability, y: Probability): Probability = x / y
}

object ProbabilityAsIfIntegral extends ProbabilityAsIfIntegral with ProbabilityOrdering

object Implicits {
  implicit object ProbabilityIsFractional extends ProbabilityIsFractional with ProbabilityOrdering { }
}

trait DiscreteProbabilityMeasure[A] extends PartialFunction[A, Probability] {
  def events: Set[A]
  def probability(a: A): Option[Probability]
  override def apply(a: A): Probability =
    probability(a) match {
      case Some(p) => p
      case None => throw new IllegalArgumentException("Event " + a + " not defined in probability measure")
    }
  override def isDefinedAt(a: A) = events.contains(a)
}
