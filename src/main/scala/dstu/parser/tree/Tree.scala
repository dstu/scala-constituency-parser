package dstu.parser.tree

import scala.annotation.tailrec

trait Tree[A, +C <: Tree[A, C]] {
  def value: A
  def children: List[C]
  override def toString: String =
    if (children.size < 1) {
      value.toString
    } else {
      "(" + value.toString + " " + children.mkString(" ") + ")"
    }
}

sealed trait NaryTree[A] extends Tree[A, NaryTree[A]]
sealed trait BinarizedTree[A] extends Tree[A, BinarizedTree[A]]

case class NaryTreeNode[A](val value: A, val children: List[NaryTree[A]]) extends NaryTree[A]

case class Leaf[A](val value: A) extends Tree[A, Nothing] with NaryTree[A] with BinarizedTree[A] {
  override def children = Nil
  override def toString = value.toString
}

object NaryTree {
  def apply[A](value: A, children: NaryTree[A]*): NaryTree[A] = NaryTree(value, children.toList)

  def apply[A](value: A, children: List[NaryTree[A]]): NaryTree[A] = {
    if (children.size > 0) {
      new NaryTreeNode[A](value, children)
    } else {
      new Leaf[A](value)
    }
  }
}

case class BinaryTreeNode[A](val value: A, val leftChild: BinarizedTree[A], val rightChild: BinarizedTree[A]) extends BinarizedTree[A] {
  private val childrenList = List(leftChild, rightChild)
  override def children = childrenList
}

case class UnaryTreeNode[A](val value: A, val child: BinarizedTree[A]) extends BinarizedTree[A] {
  private val childrenList = List(child)
  override def children = childrenList
}

object BinarizedTree {
  def apply[A](value: A): BinarizedTree[A] = new Leaf[A](value)

  def apply[A](value: A, child: BinarizedTree[A]): BinarizedTree[A] = new UnaryTreeNode[A](value, child)

  def apply[A](value: A, lc: BinarizedTree[A], rc: BinarizedTree[A]): BinarizedTree[A] = new BinaryTreeNode[A](value, lc, rc)

  def expandUnaryChain[A](value: A, intermediateChain: List[A], finalChild: BinarizedTree[A]): BinarizedTree[A] = {
    def expandChain(chain: List[A]): BinarizedTree[A] =
      chain match {
        case Nil => throw new IllegalStateException("expandChain called with no parent value")
        case head :: Nil => new UnaryTreeNode[A](head, finalChild)
        case head :: children => new UnaryTreeNode[A](head, expandChain(children))
      }

    expandChain(value :: intermediateChain)
  }
}

sealed trait Token
case class OpenParen() extends Token {
  override def toString = "("
}
case class CloseParen() extends Token {
  override def toString = ")"
}
case class Symbol(chars: String) extends Token {
  override def toString = chars
}

object TreeReader extends util.parsing.combinator.Parsers {
  type Elem = Token

  // TODO we should have a more elegant way of expressing this
  def fixifyPreterminal(s: String): String =
    s match {
      case "." => "^."
      case "," => "^,"
      case "``" => "^``"
      case "''" => "^''"
      case "$" => "^$"
      case "#" => "^#"
      case ":" => "^:"
      case "IN" => "^IN"
      case "TO" => "^TO"
      case "-LRB-" => "^-LRB-"
      case "-RRB-" => "^-RRB-"
      case _ => s
  }

  class TokenReader(val sourceString: String,
                    val tokens: Seq[Token],
                    val position: Int = 0) extends util.parsing.input.Reader[Token] {
    override def first = tokens.head
    override def pos = new util.parsing.input.OffsetPosition(sourceString, position)
    override def atEnd = tokens.isEmpty
    override def rest = new TokenReader(sourceString, tokens.tail, position + first.toString.length)
  }

  object TokenReader {
    def apply(s: String) = {
      val tokens = s.replace("(", " ( ").replace(")", " ) ").trim.split("""\s+""").toSeq.map(_ match {
        case ")" => CloseParen()
        case "(" => OpenParen()
        case x => Symbol(x)
      })
      new TokenReader(s, tokens)
    }
  }

  val openParen = accept(OpenParen())
  val closeParen = accept(CloseParen())
  val symbol = elem("symbol", { case Symbol(_) => true; case _ => false })

  val tree: Parser[NaryTree[String]] =
    openParen ~> symbol ~ rep1(tree | preTerminal) <~ closeParen ^^ { case Symbol(s) ~ children => NaryTree(s, children)
                                                                      case _ => throw new IllegalStateException}

  val preTerminal: Parser[NaryTree[String]] =
    openParen ~> symbol ~ symbol <~ closeParen ^^ { case Symbol(s) ~ Symbol(l) => NaryTree(fixifyPreterminal(s), Leaf(l))
                                                    case _ => throw new IllegalStateException }

  def apply(s: String): NaryTree[String] = {
    tree(TokenReader(s)) match {
      case Success(tree, _) => tree
      case Failure(msg, _) => throw new RuntimeException("Failure reading tree: " + msg)
      case Error(msg, _) => throw new RuntimeException("Error reading tree: " + msg)
    }
  }
}

// class NaiveTreeReader(private var tokens: Seq[Token]) {
//   def tree: NaryTree[String] = {
//     val t = readTree
//     finish
//     t
//   }

//   def readSymbol: String =
//     tokens match {
//       case Nil => throw new IllegalArgumentException("Expected: symbol. Got: end of tokens.")
//       case _ => tokens.head match {
//         case Symbol(s) => { tokens = tokens.drop(1); s }
//         case _ => throw new IllegalArgumentException("Expected: symbol. Got: " + tokens.head)
//       }
//     }

//   def readOpen: Unit = {
//     tokens match {
//       case Nil => throw new IllegalArgumentException("Expected: open. Got: end of tokens.")
//       case _ => tokens.head match {
//         case OpenParen() => { tokens = tokens.drop(1) }
//         case _ => throw new IllegalArgumentException("Expected: open. Got: " + tokens.head)
//       }
//     }
//   }

//   def readClose: Unit = {
//     tokens match {
//       case Nil => throw new IllegalArgumentException("Expected: close. Got: end of tokens.")
//       case _ => tokens.head match {
//         case CloseParen() => { tokens = tokens.drop(1) }
//         case _ => throw new IllegalArgumentException("Expected: close. Got: " + tokens.head)
//       }
//     }
//   }

//   def finish: Unit = {
//     tokens match {
//       case Nil => ()
//         case _ => throw new IllegalArgumentException("Expected: end of tokens. Got: " + tokens.head)
//     }
//   }

//   def readTree: NaryTree[String] = {
//     readOpen
//     val s = readSymbol
//     tokens match {
//       case Nil => throw new IllegalArgumentException("Expected: child. Got: end of tokens.")
//       case _ => tokens.head match {
//         case Symbol(c) => {
//           tokens = tokens.drop(1)
//           readClose
//           NaryTree(s, Leaf(c))
//         }
//         case OpenParen() => NaryTree(s, readTrees(Nil))
//         case _ => throw new IllegalArgumentException("Expected: child. Got: " + tokens.head)
//       }
//     }
//   }

//   def readTrees(trees: List[NaryTree[String]]): List[NaryTree[String]] = {
//     tokens match {
//       case Nil => throw new IllegalArgumentException("Expected: tree or close. Got: end of tokens")
//       case _ => tokens.head match {
//         case OpenParen() => {
//           val t = readTree
//           readTrees(trees :+ t)
//         }
//         case CloseParen() => {
//           tokens = tokens.drop(1)
//           trees
//         }
//         case _ => throw new IllegalArgumentException("Expected: tree or close. Got: " + tokens.head)
//       }
//     }
//   }
// }

// object NaiveTreeReader {
//   def apply(s: String): NaryTree[String] = {
//     val tokens = s.replace("(", " ( ").replace(")", " ) ").trim.split("""\s+""").toSeq.map(_ match {
//       case ")" => CloseParen()
//       case "(" => OpenParen()
//       case x => Symbol(x)
//     })
//     new NaiveTreeReader(tokens).tree
//   }
// }

// object TreeReader extends scala.util.parsing.combinator.RegexParsers {
//   import scala.util.parsing.combinator._

//   val OpenParen = """\(""".r
//   val CloseParen = """\)""".r
//   val Symbol = """[^\(\)\s]+""".r

//   def tree: Parser[NaryTree[String]] =
//     OpenParen ~> Symbol ~ rep(tree | preTerminal) <~ CloseParen ^^ { case s ~ children => NaryTree(s, children) }

//   def preTerminal: Parser[NaryTree[String]] =
//     OpenParen ~> Symbol ~ Symbol <~ CloseParen ^^ { case s ~ l => NaryTree(s, Leaf(l)) }

//   def apply(s: String): NaryTree[String] =
//     parse(tree, s) match {
//       case Success(tree, _) => tree
//       case Failure(msg, _) => throw new RuntimeException("Failure reading tree: " + msg)
//       case Error(msg, _) => throw new RuntimeException("Error reading tree: " + msg)
//     }
// }

// object TreeReader {
//   sealed trait Token
//   case class CloseParen() extends Token {
//     override def toString = ")"
//   }
//   case class OpenParen() extends Token {
//     override def toString = "("
//   }
//   case class Symbol(chars: String) extends Token {
//     override def toString = chars
//   }

//   def parse(s: String): NaryTree[String] = {
//     // messy tokenization, but it works
//     val tokens = s.replace("(", " ( ").replace(")", " ) ").trim.split("""\s+""").toSeq.map(_ match {
//       case ")" => CloseParen()
//       case "(" => OpenParen()
//       case x => Symbol(x)
//     })

//     tokens match {
//       case Nil => throw new IllegalArgumentException("Expected: tree. Got: end of tokens.")
//       case _ => tree(finish)(tokens)
//     }
//   }

//   def tree(next: NaryTree[Sring] => Seq[Token] => NaryTree[String])(tokens: Seq[Token]): NaryTree[String] =
//     (openParen andThen symbol andThen trees andThen closeParen)(next)(tokens)
//     // openParen(symbol(trees(closeParen(next))))(tokens)

//   def finish(t: NaryTree[String])(tokens: Seq[Token]): NaryTree[String] =
//     tokens match {
//       case Nil => t
//       case _ => throw new IllegalArgumentException("Expected: nothing. Got: " + tokens.head)
//     }

//   def openParen(next: Seq[Token] => NaryTree[String])(tokens: Seq[Token]): NaryTree[String] =
//     tokens match {
//       case Nil => throw new IllegalArgumentException("Expected: '('. Got: nothing.")
//       case _ => tokens.head match {
//         case OpenParen() => next(tokens.tail)
//         case _ => throw new IllegalArgumentException("Expected: '('. Got: " + tokens.head)
//       }
//     }

//   def symbol(next: String => Seq[Token] => NaryTree[String])(tokens: Seq[Token]): NaryTree[String] =
//     tokens match {
//       case Nil => throw new IllegalArgumentException("Expected: symbol. Got: nothing.")
//       case _ => tokens.head match {
//         case Symbol(chars) => next(chars)(trees(tokens.tail))
//         case _ => throw new IllegalArgumentException("Expected: symbol. Got: " + tokens.head)
//       }
//     }

//   def trees(next: List[NaryTree[String]](NaryTree[String], Seq[Token]) => NaryTree[String])(x: String, tokens: Seq[Token], tt: List[NaryTree[String]] = Nil): NaryTree[String] =
//     tokens match {
//       case Nil => throw new IllegalArgumentException("Expected: child trees. Got: nothing.")
//       case _ => tokens.head match {
//         case OpenParen() => tree(
        

//   def subTree(next: 

//   val subTree: PartialFunction[(, Seq[Token]), NaryTree[String]] = {
//     case Nil => throw new IllegalArgumentException("Expected: tree. Got: nothing.")
//     case tokens => {
//       tokens.head match {
//         case OpenParen() => symbol
//     }
//   }

//   def tree(tokens: Seq[Token]): NaryTree[String] = subTree(finish, tokens)

//   def openParen(next: Seq[Token] => NaryTree[String])(tokens: Seq[Token]): NaryTree[String] = {
//     tokens match {
//       case Nil => throw new IllegalArgumentException("Expected: '('. Got: end of tokens.")
//       case _ => {
//         tokens.head match {
//           case OpenParen() => next(tokens.tail)
//           case _ => throw new IllegalArgumentException("Expected: '('. Got: " + tokens.head)
//         }
//       }
//     }
//   }

//   def symbol(next: (String, Seq[Token]) => NaryTree[String])(tokens: Seq[Token]): NaryTree[String] = {
//     tokens match {
//       case Nil => throw new IllegalArgumentException("Expected: symbol. Got: end of tokens.")
//       case _ => tokens.head match {
//         case Symbol(chars) => next(chars, tokens.tail)
//         case _ => throw new IllegalArgumentException("Expected: symbol. Got: " + tokens.head)
//       }
//     }
//   }

//   def subTree(next: (NaryTree[String], Seq[Token]) => NaryTree[String])(s: String, tokens: Seq[Token]): NaryTree[String] = {
//     tokens match {
//       case Nil => throw new IllegalArgumentException("Expected: tree children. Got: end of tokens.")
//       case _ => tokens.head match {
//         case Symbol(chars) => next(NaryTree(s, NaryTree(chars)), tokens.tail)
//         case OpenParen() => NaryTree(s, symbol(children(closeParen)(_, _))(tokens.tail))
//         case _ => throw new IllegalArgumentException("Expected: tree children. Got: " + tokens.head)
//       }
//     }
//   }

//   def childrenList(next: (List[NaryTree[String]], Seq[Token]) => NaryTree[String])(l: List[NaryTree[String]])(c: NaryTree[String], tokens: Seq[Token]): NaryTree[String] = {
//     tokens match {
//       case Nil => throw new IllegalArgumentException("Expected: ')' or '('. Got: end of tokens.")
//       case _ => tokens.head match {
//         case CloseParen() => next(l :+ c, tokens.tail)
//         case OpenParen() => symbol(subTree(childrenList(next)(l :+ c)(_))(_))(tokens.tail)
//         case _ => throw new IllegalArgumentException("Expected: ')' or '('. Got: " + tokens.head)
//       }
//     }
//   }

//   def closeParen(next: NaryTree[String] => NaryTree[String])(tree: NaryTree[String], tokens: Seq[Token]): NaryTree[String] = {
//     tokens match {
//       case Nil => throw new IllegalArgumentException("Expected: ')'. Got: end of tokens.")
//       case _ => tokens.head match {
//         case CloseParen() => next(tree)
//         case _ => throw new IllegalArgumentException("Expected: ')'. Got: " + tokens.head)
//       }
//     }
//   }

//   def finish(tree: NaryTree[String])(tokens: Seq[Token]): NaryTree[String] = {
//     tokens match {
//       case Nil => tree
//       case _ => throw new IllegalArgumentException("Expected: end of tokens. Got: " + tokens.head)
//     }
//   }

//   def apply(s: String): NaryTree[String] = parse(s)
// }

object Tree {
  def rightBinarize(root: NaryTree[String]): BinarizedTree[String] =
    rightBinarize((a: Traversable[String]) => a.mkString("_"),
      root)

  def leftBinarize(root: NaryTree[String]): BinarizedTree[String] =
    leftBinarize((a: Traversable[String]) => a.mkString("_"),
      root)

  def lift(root: BinarizedTree[String]): NaryTree[String] =
    lift((a: String) => a.contains('_'), root)

  def rightBinarize[A](uniqueBinary: Traversable[A] => A,
    root: NaryTree[A]): BinarizedTree[A] = {
    def binarize(root: NaryTree[A]): BinarizedTree[A] =
      root match {
        case l @ Leaf(_) => l
        case NaryTreeNode(value, children) =>
          children.size match {
            case 0 => BinarizedTree(value)
            case 1 => BinarizedTree(value, binarize(children.head))
            case 2 => BinarizedTree(value, binarize(children.head), binarize(children.last))
            case _ => {
              val remChildren = children.drop(1)
              val newSymbol = uniqueBinary(remChildren.map(_.value))
              val lc = binarize(children.head)
              val rc = binarize(NaryTree(newSymbol, remChildren))
              BinarizedTree(value, lc, rc)
            }
          }
      }

    binarize(root)
  }

  def leftBinarize[A](uniqueBinary: Traversable[A] => A,
    root: NaryTree[A]): BinarizedTree[A] = {
    def binarize(root: NaryTree[A]): BinarizedTree[A] =
      root match {
        case l @ Leaf(_) => l
        case NaryTreeNode(value, children) =>
          children.size match {
            case 0 => BinarizedTree(value)
            case 1 => BinarizedTree(value, binarize(children.head))
            case 2 => BinarizedTree(value, binarize(children.head), binarize(children.last))
            case _ => {
              val remChildren = children.dropRight(1)
              val newSymbol = uniqueBinary(remChildren.map(_.value))
              val lc = binarize(NaryTree(newSymbol, remChildren))
              val rc = binarize(children.last)
              BinarizedTree(value, lc, rc)
            }
          }
      }

    binarize(root)
  }

  def lift[A](liftp: A => Boolean, root: BinarizedTree[A]): NaryTree[A] = {
    root match {
      case Leaf(value) => NaryTree(value)
      case UnaryTreeNode(value, child) => NaryTree(value, lift(liftp, child))
      case BinaryTreeNode(value, leftChild, rightChild) => {
        var lc = List(lift(liftp, leftChild))
        if (liftp(leftChild.value)) {
          lc = lc.flatMap(_.children)
        }

        var rc = List(lift(liftp, rightChild))
        if (liftp(rightChild.value)) {
          rc = rc.flatMap(_.children)
        }

        NaryTree(value, lc ++ rc)
      }
    }
  }

  def read(source: String): NaryTree[String] = TreeReader(source)
}
