package dstu.parser

import dstu.parser.tree._
import dstu.parser.math.Probability

trait Production[A] {
  def parent: A
  def children: List[A]
  def expand: Tree[A, _]
}

sealed trait BinarizedProduction[A] extends Production[A] {
  override def expand: BinarizedTree[A]
}

case class BinaryProduction[A](val parent: A,
  val leftChild: A,
  val rightChild: A) extends BinarizedProduction[A] {
  private val childrenList = List(leftChild, rightChild)
  override def children: List[A] = childrenList
  override def expand = BinarizedTree(parent, BinarizedTree(leftChild), BinarizedTree(rightChild))
  override def toString = parent.toString + " --> " + leftChild.toString + " " + rightChild.toString
}

case class UnaryProduction[A](val parent: A,
                              val child: A,
                              val intermediate: List[A] = Nil) extends BinarizedProduction[A] {
  private val childrenList = List(child)
  def isChain: Boolean = !intermediate.isEmpty
  override def children: List[A] = childrenList
  override def expand =
    if (isChain) {
      def expand(chain: List[A]): BinarizedTree[A] =
        if (chain.size < 2) {
          BinarizedTree(chain.head)
        } else {
          BinarizedTree(chain.head, expand(chain.tail))
        }
      expand((parent :: intermediate) :+ child)
    } else {
      BinarizedTree(parent, BinarizedTree(child))
    }
  override def toString = {
    val middle = if (isChain) { "(" + intermediate.mkString(" ") + ")" } else { "" }
    parent.toString + " -> " + middle + child.toString
  }
}

object BinarizedProduction {
  def apply[A](parent: A, child: A): UnaryProduction[A] = new UnaryProduction[A](parent, child, Nil)

  def apply[A](chain: List[A]): UnaryProduction[A] =
    chain match {
      case parent :: children => UnaryProduction(parent, children.last, children.dropRight(1))
      case _ => throw new IllegalArgumentException("Invalid unary production chain: " + chain)
    }

  def apply[A](parent: A, child: A, intermediate: List[A]): UnaryProduction[A] = UnaryProduction(parent, child, intermediate)

  def apply[A](parent: A, lc: A, rc: A): BinaryProduction[A] = new BinaryProduction[A](parent, lc, rc)
}

trait Grammar[A, Prod <: Production[A]] {
  def startSymbol: A
  def terminalSymbols: Set[A]
  def nonTerminalSymbols: Set[A]
  def productions: Set[Prod]
}

class BinarizedGrammar[A](val startSymbol: A,
  val terminalSymbols: Set[A],
  val nonTerminalSymbols: Set[A],
  val binaryProductions: Set[BinaryProduction[A]],
  val unaryProductions: Set[UnaryProduction[A]]) extends Grammar[A, BinarizedProduction[A]] {
  override def productions: Set[BinarizedProduction[A]] = binaryProductions ++ unaryProductions
}

object BinarizedGrammar {
  def apply[A](startSymbol: A,
    terminalSymbols: Set[A],
    nonTerminalSymbols: Set[A],
    binaryProductions: Set[BinaryProduction[A]],
    unaryProductions: Set[UnaryProduction[A]]) =
    new BinarizedGrammar[A](startSymbol, terminalSymbols, nonTerminalSymbols, binaryProductions, unaryProductions)

  def apply[A](startSymbol: A,
    binaryProductions: Set[BinaryProduction[A]],
    unaryProductions: Set[UnaryProduction[A]]): BinarizedGrammar[A] = {
    val productions = binaryProductions ++ unaryProductions
    val childSymbols: Set[A] = productions.foldLeft(Set[A]())((acc, p) => acc ++ p.children)
    val parentSymbols: Set[A] = productions.map(_.parent)
    val terminalSymbols = childSymbols -- parentSymbols
    val nonTerminalSymbols = (childSymbols ++ parentSymbols) -- terminalSymbols
    BinarizedGrammar(startSymbol, terminalSymbols, nonTerminalSymbols, binaryProductions, unaryProductions)
  }
}

class BinarizedPcfg[A](startSymbol: A,
  terminalSymbols: Set[A],
  nonTerminalSymbols: Set[A],
  binaryProductions: Set[BinaryProduction[A]],
  unaryProductions: Set[UnaryProduction[A]],
  val probabilities: Map[BinarizedProduction[A], Probability]) extends BinarizedGrammar[A](startSymbol,
  terminalSymbols,
  nonTerminalSymbols,
  binaryProductions,
  unaryProductions) {
  val unaryProbabilities: Map[(A, A), Probability] = unaryProductions.map(prod => {
    val prob = probabilities(prod)
    Pair((prod.parent, prod.child), prob)
  }).toMap

  val binaryProbabilities: Map[(A, A, A), Probability] = binaryProductions.map(prod => {
    val prob = probabilities(prod)
    Pair((prod.parent, prod.leftChild, prod.rightChild), prob)
  }).toMap

  def apply(a: UnaryProduction[A]): Probability = probabilities(a)

  def apply(a: BinaryProduction[A]): Probability = probabilities(a)

  def apply(p: A, c: A): Probability = unaryProbabilities.getOrElse((p, c), Probability.Zero)

  def apply(p: A, lc: A, rc: A): Probability = binaryProbabilities.getOrElse((p, lc, rc), Probability.Zero)
}

// trait Counter[A, D: Fractional] {
//   val Zero = implicitly[Fractional[D]].zero

//   def apply(key: A): D = getOrElse(key, Zero)

//   def get(key: A): Option[D]

//   def normalize(implicit conversion: D => Probability): Map[A, Probability] = {
//     val totals = productions.foldLeft(Map[A, D]())((acc, prod) => {
//       val currentValue = acc.getOrElse(prod.parent, Zero)
//       val score = Counter.this(prod)
//       acc + Pair(prod.parent, currentValue + score)
//     })
//     productions.foldLeft(Map[Prod, Probability]())((acc, prod) => {
//       val score = Counter.this(prod)
//       Pair(prod, conversion(implicitly[Fractional[A]].div(score, totals(prod.parent))))
//     })
//   }

//   def +(value: (A, D)): this.type

//   def ++(values: Traversable[(A, D)]): this.type

//   def -(value: (A, D)): this.type

//   def --(value: (A, D)): this.type
// }

object BinarizedPcfg {
  def apply[A](startSymbol: A,
    terminalSymbols: Set[A],
    nonTerminalSymbols: Set[A],
    binaryProductions: Set[BinaryProduction[A]],
    unaryProductions: Set[UnaryProduction[A]],
    probs: Map[BinarizedProduction[A], Probability]): BinarizedPcfg[A] =
    new BinarizedPcfg[A](startSymbol, terminalSymbols, nonTerminalSymbols, binaryProductions, unaryProductions, probs)

  def apply[A](startSymbol: A,
    trees: Traversable[BinarizedTree[A]]): BinarizedPcfg[A] = {
    val terminalSymbols = new scala.collection.mutable.HashSet[A]
    val nonTerminalSymbols = new scala.collection.mutable.HashSet[A]
    val unaryProductions = new scala.collection.mutable.HashSet[UnaryProduction[A]]
    val binaryProductions = new scala.collection.mutable.HashSet[BinaryProduction[A]]
    val productionCounts = new scala.collection.mutable.HashMap[BinarizedProduction[A], Int]
    val normalizingCounts = new scala.collection.mutable.HashMap[A, Int]

    def accumulate(tree: BinarizedTree[A]): Unit = {
        tree match {
          case Leaf(value) => terminalSymbols += value
          case UnaryTreeNode(value, child) => {
            nonTerminalSymbols += value
            val prod = UnaryProduction(value, child.value)
            unaryProductions += prod
            productionCounts += Pair(prod, productionCounts.getOrElse(prod, 0) + 1)
            normalizingCounts += Pair(value, normalizingCounts.getOrElse(value, 0) + 1)
            accumulate(child)
          }
          case BinaryTreeNode(value, leftChild, rightChild) => {
            // ASSUMPTION OF TREEBANK-STYLE GRAMMAR HERE
            nonTerminalSymbols += value
            val prod = BinarizedProduction(value, leftChild.value, rightChild.value)
            binaryProductions += prod
            productionCounts += Pair(prod, productionCounts.getOrElse(prod, 0) + 1)
            normalizingCounts += Pair(value, normalizingCounts.getOrElse(value, 0) + 1)
            accumulate(leftChild)
            accumulate(rightChild)
          }
          case _ => throw new IllegalArgumentException("Unrecognized tree node: " + tree)
        }
    }
    trees.foreach(accumulate(_))

    val probabilities = for (
      prod <- unaryProductions ++ binaryProductions;
      prodCount = productionCounts.getOrElse(prod, 0).toDouble;
      parentNorm = normalizingCounts.getOrElse(prod.parent, 0).toDouble;
      probability = Probability(prodCount / parentNorm)
    ) yield Pair(prod, probability)
    // println("nonterminal symbols:")
    // println(nonTerminalSymbols)
    // println("terminal symbols:")
    // println(terminalSymbols)

    BinarizedPcfg(startSymbol, terminalSymbols.toSet, nonTerminalSymbols.toSet, binaryProductions.toSet, unaryProductions.toSet, probabilities.toMap)
  }

  def load(startSymbol: String, file: java.io.File): BinarizedPcfg[String] = {
    val unaryRule  = """(\d+) (\S+)\s+-->\s+(\S+)""".r
    val binaryRule = """(\d+) (\S+)\s+-->\s+(\S+)\s+(\S+)""".r

    val terminalSymbols = new collection.mutable.HashSet[String]
    val nonTerminalSymbols = new collection.mutable.HashSet[String]
    val binaryProductions = new collection.mutable.HashSet[BinaryProduction[String]]
    val unaryProductions = new collection.mutable.HashSet[UnaryProduction[String]]
    val energies = new collection.mutable.HashMap[BinarizedProduction[String], Double]
    val normalizingCounts = new collection.mutable.HashMap[String, Double]

    io.Source.fromFile(file).getLines.foreach({
      case unaryRule(count, parent, child) => {
        val u = UnaryProduction(parent, child)
        val c = count.toDouble
        unaryProductions += u
        energies += Pair(u, c)
        normalizingCounts += Pair(parent, c + normalizingCounts.getOrElse(parent, 0.0))
        println("Read unary rule " + u)
      }
      case binaryRule(count, parent, lc, rc) => {
        val b = BinaryProduction(parent, lc, rc)
        val c = count.toDouble
        binaryProductions += b
        energies += Pair(b, c)
        normalizingCounts += Pair(parent, c + normalizingCounts.getOrElse(parent, 0.0))
        println("Read binary rule " + b)
      }
    })

    val normalizedProbabilities = energies.map(x => (x._1, Probability(x._2 / normalizingCounts.getOrElse(x._1.parent, 0.0)))).toMap

    new BinarizedPcfg[String](startSymbol,
                              terminalSymbols.toSet,
                              nonTerminalSymbols.toSet,
                              binaryProductions.toSet,
                              unaryProductions.toSet,
                              normalizedProbabilities)
  }
}

package transform {
  object UnaryCollapse {
    def apply[A](grammar: BinarizedPcfg[A]): BinarizedPcfg[A] = {
      val symbols = grammar.unaryProductions.map(_.parent).toList
      val path = new collection.mutable.HashMap[(A, A), Probability]
      val next = new collection.mutable.HashMap[(A, A), A]
      grammar.unaryProductions.filter(p => symbols.contains(p.child)).foreach(p => path += Pair(Pair(p.parent, p.child), grammar(p)))

      def probability(i: A, j: A): Probability = path.getOrElse(Pair(i, j), Probability.Zero)

      def updatePath(i: A, j: A, k: A, production: Probability): Unit = {
        path += Pair(Pair(i, j), production)
        next += Pair(Pair(i, j), k)
      }

      for (
        k <- symbols;
        i <- symbols;
        if k != i;
        j <- symbols;
        if j != i && j != k;
        ij = probability(i, j);
        ik = probability(i, k);
        kj = probability(k, j);
        product = ik * kj;
        if product > ij
      ) yield { updatePath(i, j, k, product) }

      def extractPath(i: A, j: A): List[A] =
        probability(i, j) match {
          case Probability.Zero => Nil
          case _ =>
            next.get((i, j)) match {
              case None => List(i, j)
              case Some(intermediate) => (extractPath(i, intermediate) ++ extractPath(intermediate, j).tail)
            }
        }

      val newProductions: Map[UnaryProduction[A], Probability] =
        (for (
          i <- symbols;
          j <- symbols;
          if i != j;
          chain = extractPath(i, j);
          if chain != Nil
        ) yield Pair(BinarizedProduction(chain), probability(i, j))).toMap

      BinarizedPcfg(grammar.startSymbol,
                    grammar.terminalSymbols,
                    grammar.nonTerminalSymbols,
                    grammar.binaryProductions,
                    grammar.unaryProductions ++ newProductions.keys,
                    grammar.probabilities ++ newProductions)
    }
  }
}
