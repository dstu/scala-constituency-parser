package dstu.parser.cky

import dstu.parser._
import dstu.parser.tree._
import dstu.parser.math.Probability
import scala.annotation.tailrec

case class Span(start: Int, end: Int)

object Parser {
  def apply[A](grammar: BinarizedPcfg[A]): Parser[A] = new Parser[A](grammar)

  def parseTree[A](grammar: BinarizedPcfg[A], tokens: Seq[A]): Option[BinarizedTree[A]] =
    Parser(grammar).parseTree(tokens)
}

class Parser[A](val grammar: BinarizedPcfg[A]) {
  sealed trait CellValue {
    def symbol: A
    def probability: Probability
    def toTree: BinarizedTree[A]
  }

  case class LeafCellValue(val symbol: A) extends CellValue {
    override def probability = Probability.One
    override def toTree = BinarizedTree(symbol)
  }

  case class UnaryCellValue(val symbol: A,
    val probability: Probability,
                            val intermediateChain: List[A],
    val backPointer: CellValue) extends CellValue {
    override def toTree = BinarizedTree.expandUnaryChain(symbol, intermediateChain, backPointer.toTree)
  }

  case class BinaryCellValue(val symbol: A,
    val probability: Probability,
    val leftBackPointer: CellValue,
    val rightBackPointer: CellValue) extends CellValue {
    override def toTree =
      BinarizedTree(symbol, leftBackPointer.toTree, rightBackPointer.toTree)
  }

  type ParseChart = Map[Span, Iterable[CellValue]]

  private val byChildren: Map[Pair[A, A], Set[BinaryProduction[A]]] =
    (for (production <- grammar.binaryProductions;
          key = Pair(production.leftChild, production.rightChild))
     yield
       Pair(key, production)).foldLeft(Map[(A, A), Set[BinaryProduction[A]]]())((acc, p) => {
         acc + Pair(p._1, acc.getOrElse(p._1, Set[BinaryProduction[A]]()) + p._2)
       })

  private val byChild: Map[A, Set[UnaryProduction[A]]] =
    (for (production <- grammar.unaryProductions;
          key = production.child)
      yield Pair(key, production)).foldLeft(Map[A, Set[UnaryProduction[A]]]())((acc, p) => {
        acc + Pair(p._1, acc.getOrElse(p._1, Set[UnaryProduction[A]]()) + p._2)
      })

  private def withChildren(lc: A, rc: A) = byChildren.getOrElse(Pair(lc, rc), Nil.toSet)

  private def withChild(c: A) = byChild.getOrElse(c, Nil.toSet)

  final def expandUnary(lookup: Iterable[CellValue]): Iterable[CellValue] = {
    val newValues = for (
      child <- lookup;
      prod <- withChild(child.symbol);
      // TODO we probably shouldn't need this, but it is used to
      // get rid of oddities surrounding preterminal productions
      // like , -> , and . -> .
      if child.symbol != prod.parent;
      probability = grammar(prod) * child.probability
    ) yield UnaryCellValue(prod.parent, probability, prod.intermediate, child)
    lookup ++ newValues
  }

  final def expandBinary(leftLookup: Iterable[CellValue], rightLookup: Iterable[CellValue]): Iterable[CellValue] =
    for (
      leftChild <- leftLookup;
      rightChild <- rightLookup;
      prod <- withChildren(leftChild.symbol, rightChild.symbol);
      probability = grammar(prod) * leftChild.probability * rightChild.probability// ;
      // foo = println("Considering binary values over " + leftChild + " and " + rightChild)
    ) yield {
      // println("Possible binary cell value: (" + prod.parent + " " + leftChild + "  " + rightChild + ")")
      BinaryCellValue(prod.parent, probability, leftChild, rightChild)
    }

  def condense(lookup: Iterable[CellValue]): Iterable[CellValue] =
    lookup.foldLeft(Map[A, CellValue]())((acc, v) => {
      val extant = acc.getOrElse(v.symbol, v)
      if (v.probability >= extant.probability) {
        acc + Pair(v.symbol, v)
      } else {
        acc
      }
    }).values.toList

  def fillCell(chart: ParseChart, span: Span): ParseChart = {
    // println("Filling span " + span)
    // println("Chart so far: ")
    // printSpans(chart.toList)
    val cellContent = for (
      mid <- span.start + 1 until span.end;
      leftChild = chart.getOrElse(Span(span.start, mid), Nil);
      if !leftChild.isEmpty;
      // foo = println("Left child over " + Span(span.start, mid) + ": " + leftChild);
      rightChild = chart.getOrElse(Span(mid, span.end), Nil);
      if !rightChild.isEmpty// ;
      // bar = println("Right child over " + Span(mid, span.end) + ": " + rightChild)
    ) yield expandUnary(condense(expandBinary(leftChild, rightChild)))
    // println("Just filled span " + span + "; content: " + cellContent)
    if (cellContent.isEmpty) {
      chart
    } else {
      chart + Pair(span, condense(cellContent.reduce(_ ++ _)))
    }
  }

  def fillPreterminal(chart: ParseChart, span: Span): Pair[Span, Iterable[CellValue]] =
    Pair(span, condense(expandUnary(expandUnary(chart.getOrElse(span, Nil)))))

  def initChart(tokens: Seq[A]): ParseChart =
    (for (
      (t, i) <- tokens zip (0 until tokens.size);
      value = LeafCellValue(t)
    ) yield Pair(Span(i, i + 1), List(value))).toMap

  def parse(tokens: Seq[A]): ParseChart = {
    @tailrec
    def parse(spanLength: Int, chart: ParseChart): ParseChart =
      if (spanLength > tokens.size) {
        chart
      } else if (spanLength == 1) {
        val filledChart = chart ++ (for (
          start <- 0 until tokens.size;
          span = Span(start, start + 1)
        ) yield fillPreterminal(chart, span))
        parse(spanLength + 1, filledChart)
      } else {
        val filledChart = (for (
          start <- 0 to tokens.size - spanLength;
          span = Span(start, start + spanLength)
        ) yield fillCell(chart, span)).reduce(_ ++ _)
        parse(spanLength + 1, filledChart)
      }

    parse(1, initChart(tokens))
  }

  def printSpans(spans: Iterable[(Span, Iterable[CellValue])]): Unit = {
    implicit object chartOrdering extends Ordering[(Span, Iterable[CellValue])] {
      override def compare(ap: (Span, Iterable[CellValue]), bp: (Span, Iterable[CellValue])): Int = {
        val a = ap._1
        val b = bp._1
        if (a.start == b.start) {
          (a.end - a.start) - (b.end - b.start)
        } else {
          a.start - b.start
        }
      }
    }
    util.Sorting.stableSort(spans.toList).foreach(x => println(x._1 + ": " + x._2))
  }

  def parseTree(tokens: Seq[A]): Option[BinarizedTree[A]] = {
    val chart = parse(tokens)
    // println("Filled parse chart:")
    // printSpans(chart.toList)
    
    val topLookup = chart.get(Span(0, tokens.size))
    topLookup match {
      case Some(lookup) => {
        val l = lookup.filter(_.symbol == grammar.startSymbol)
        if (l.size > 0) {
          Some(l.head.toTree)
        } else {
          None
        }
      }
      case None => None
    }
  }
}
