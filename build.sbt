name := "Scala constituency parser"

version := "0.2-SNAPSHOT"

scalaVersion := "2.9.1"

libraryDependencies += "junit" % "junit" % "4.8" % "test"

scalacOptions += "-deprecation"

scalacOptions += "-unchecked"
